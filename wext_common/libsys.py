#Lib: not executable
# -*- coding: UTF-8 -*-

import os

if os.environ.get("WEXT_COMMON_DEBUG"):
    print("import >>>", __file__)

import platform
import os
import sys

from .libcommon import *

def getPlatform():
    ret = platform.system().lower()
    if ret.startswith("windows"):
        return "windows"
    elif ret.startswith("darwin"):
        return "darwin"
    else:
        return ret
    
def isWin():
    return "windows" == getPlatform()
