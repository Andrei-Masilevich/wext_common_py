#Lib: not executable
# -*- coding: UTF-8 -*-

import os

if os.environ.get("WEXT_COMMON_DEBUG"):
    print("import >>>", __file__)

import sys
import getpass

from .libtrace import *

def input2(txt):
    if isPython3():
        return input(txt)
    else:
        print(txt)
        return raw_input()

def anyKey(txt = "Press any key..."):
    return input2(txt)

@debug
def PAUSE(txt = None):
    if not txt:
        return anyKey()
    else:
        return anyKey(txt)

def ask(text):
    while (text.endswith('?')):
        text = text[:-1]

    if len(text):
        inputText = "%s [Y/n]?" % text
    else:
        inputText = text

    if (not inputText.endswith('?')):
        inputText = inputText + '?'

    while (True):
        result = input2(inputText + "  ").lower()
        if (not len(result)):
            break
        else:
            if ('y' == result):
                break
            elif ('n' == result):
                return False
    return True

def askPassword(txt = "Password:"):
    return getpass.getpass(txt)
