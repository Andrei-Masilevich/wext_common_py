#Lib: not executable
# -*- coding: UTF-8 -*-

import os

if os.environ.get("WEXT_COMMON_DEBUG"):
    print("import >>>", __file__)

import sys
import time

from .libcommon import *

def tracing(func):
    def _tracing(*args,**kwargs):    
        if isPython3():
            global trace_env
        if __debug__ and trace_env.debug:
            def callf(*args,**kwargs):
                from .liblog import LDBG

                LDBG("%s(%r, %r)", func.__name__, args, kwargs)
                r = func(*args,**kwargs)
                LDBG("%s -> %r", func.__name__, r)
                
                return r
            return callf(*args,**kwargs)
        else:
            return func
    return _tracing

def debug(func):
    def _debug(*args,**kwargs):
        if isPython3():
            global trace_env
        if trace_env.debug:
            return func(*args,**kwargs)
        else:
            pass
    return _debug

def release(func):
    def _release(*args,**kwargs):
        if isPython3():
            global trace_env
        if not trace_env.debug:
            return func(*args,**kwargs)
        else:
            pass
    return _release

@debug
def SLEEP(seconds):
    return time.sleep(seconds)

class profiler(object):
    def __init__(self):
        self._init = False

    def __enter__(self):
        self.start()
        return self

    def __exit__(self, type, value, traceback):
        self.end()
        if __debug__ and value:
            sys.stderr.write("Exception {0} = {1} in 'with'".format(type, value))

    def elapsed(self, prec = 0):
        if prec>0:
            str="{0:.%df}" % prec
        else:
            str="{0:.0f}"
        return str.format(self._elapsed)

    def _time(self, tm):
        return time.strftime("%x %X", time.localtime(tm))

    def __bool__(self):
        return self._init
        
    def start(self):
        self._init = True
        self._startTime = time.time()
        self._finishTime = self._startTime
        self._elapsed = 0
    def end(self):
        self._finishTime = time.time()
        self._elapsed = self._finishTime - self._startTime
        
    def started(self):
        return self._time(self._startTime)

    def finished(self):
        return self._time(self._finishTime)



