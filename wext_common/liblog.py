#Lib: not executable
# -*- coding: UTF-8 -*-

import os

if os.environ.get("WEXT_COMMON_DEBUG"):
    print("import >>>", __file__)

import logging as log
import datetime as dt

from .libtrace import *
from .libsys import isWin

class WFormatter(log.Formatter):
    converter=dt.datetime.fromtimestamp
    
    def __init__(self, 
                 fmt, datefmt = None,
                 print_date = True):
        super(WFormatter, self).__init__( fmt, datefmt)
        self._print_date = print_date
        
    def formatTime(self, record, datefmt=None):
        ct = self.converter(record.created)
        if datefmt:
            s = ct.strftime(datefmt)
        else:
            if self._print_date:
                t = ct.strftime("%d.%m.%Y %H:%M:%S")
            else:
                t = ct.strftime("%H:%M:%S")
            s = "%s:%03d" % (t, record.msecs)
        return s

class WFormatter2(WFormatter):
    
    def __init__(self, 
                 fmt = "=%(levelname)s=" + \
                " %(asctime)s \"%(name)s\"" + \
                " (%(thread)d:%(threadName)s)>" + \
                " %(message)s",
                 fmt4debug = "<" + \
                " %(asctime)s >" + \
                " %(message)s",
                 fmt4info = "%(message)s"):
        super(WFormatter2, self).__init__( fmt, print_date = False)
        self._fmt4debug = WFormatter(fmt4debug, print_date = False)
        self._fmt4info = WFormatter(fmt4info, print_date = False)
    
    def format(self, record):
        
        if record.levelno == log.NOTSET or \
               record.levelno == log.INFO or \
               record.levelno == log.INFO - 1:
            return self._fmt4info.format(record)
        elif record.levelno == log.DEBUG or \
               record.levelno == log.DEBUG - 1:
            return self._fmt4debug.format(record)
            
        return super(WFormatter2, self).format(record)
    
class WLogger(log.Logger):
    _formatter = WFormatter(
                "<%(levelname)s" + \
                " %(asctime)s \"%(name)s\"" + \
                " (%(thread)d:%(threadName)s)>" + \
                " %(message)s" + \
                " (from %(filename)s:%(lineno)d)")

    def __init__(self, name = "", levelOpt = None):
        if not levelOpt:
            if trace_env.debug:
                level = log.DEBUG
            else:
                level = log.INFO
        else:
            level = levelOpt
        if name == trace_env.app_name or trace_env.app_name == '*':
            self._logname = name
        else:
            self._logname = trace_env.app_name
            if len(self._logname) and len(name):
                self._logname += "."
                self._logname += name
        if not len(self._logname):
            raise AttributeError

        super(WLogger, self).__init__( self._logname, level)
        #Change default propagation behavior!
        self.propagate = False
        
    def _apply4Handler(self, hd, fmt, level):
        if isinstance(fmt, log.Formatter):
            hd.setFormatter(fmt)
        else:
            if len(fmt):
                fmtObj = WFormatter(fmt)
            else:
                fmtObj = self._formatter
            hd.setFormatter(fmtObj)
            hd.setLevel(level)
            
    def addRotatingFileHandler(self, 
            filename = "", 
            maxBytes = 100*1024*1024, 
            backupCount = 10,
            fmt = "",
            level = log.NOTSET):
        import logging.handlers as loghs

        if not len(filename):
            filename = self._logname
 
        hd = loghs.RotatingFileHandler(filename, maxBytes = maxBytes, backupCount = backupCount)
        
        self._apply4Handler(hd, fmt, level)
        super(WLogger, self).addHandler(hd)

    def addLocalSysLogHandler(self, 
            fmt = "%(name)s " + \
                  "<%(levelname)s %(asctime)s> " + \
                  "%(message)s",
            level = log.NOTSET):
        import logging.handlers as loghs
        
        if isWin():
            #need Python extensions for Windows (pywin32): win32service.pyd
            #
            hd = loghs.NTEventLogHandler(self._logname, logtype='Application')
        else:
            if sys.platform == "darwin":
                syslog_address = "/var/run/syslog"
            else:
                syslog_address = "/dev/log"
            
            if not os.path.exists(syslog_address):
                raise NotImplemented
            
            hd = loghs.SysLogHandler(address= syslog_address, facility=loghs.SysLogHandler.LOG_USER)
        
        self._apply4Handler(hd, fmt, level)
        super(WLogger, self).addHandler(hd)
        
    def addStreamHandler(self, 
            fmt = "",
            stream=sys.stderr,
            level = log.NOTSET):
        hd = log.StreamHandler(stream)
        
        self._apply4Handler(hd, fmt, level)
        super(WLogger, self).addHandler(hd)
    
_liblog = None
if not _liblog:
    _level = log.DEBUG
    if trace_env.debug:
        _level -= 1

    log.addLevelName(log.DEBUG, "D")
    log.addLevelName(log.INFO, "M")
    log.addLevelName(log.WARNING, "W")
    log.addLevelName(log.ERROR, "E")
    log.addLevelName(log.CRITICAL, "EE")
    log.addLevelName(log.DEBUG - 1, "DEBUG")
    log.addLevelName(log.INFO - 1, "INFO")
    log.addLevelName(log.WARNING - 1, "WARNING")
    log.addLevelName(log.ERROR - 1, "ERROR")
    log.basicConfig(
            format="%(message)s",
            level=_level)
    log.setLoggerClass(WLogger)

    _liblog = log.getLogger("common")
    _liblog.addStreamHandler(WFormatter2())
    _liblog.setLevel(_level);

def _LOG(lv, msg, *args, **settings):
    _liblog.log(lv-1, msg, *args)
    _log = settings.get("log")
    if _log:
        _prefix = settings.get("prefix")
        if _prefix:
            _msg = "%r " + msg
            _args = [_prefix]
            _args.extend(args)
            _log.log(lv, _msg, *_args)
        else:
            _log.log(lv, msg, *args)

@debug
def LDBG(msg, *args, **settings):
    _LOG(log.DEBUG, msg, *args, **settings)

def LMSG(msg, *args, **settings):
    _LOG(log.INFO, msg, *args, **settings)

def LWRN(msg, *args, **settings):
    _LOG(log.WARNING, msg, *args, **settings)

def LERR(msg, *args, **settings):
    _LOG(log.ERROR, msg, *args, **settings)
