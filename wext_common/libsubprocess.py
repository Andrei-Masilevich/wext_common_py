#Lib: not executable
# -*- coding: UTF-8 -*-

import os

if os.environ.get("WEXT_COMMON_DEBUG"):
    print("import >>>", __file__)

import subprocess as pp

from .liblog import *

@tracing
def _splitPathWithArgs( pathWithArgs ):
    lstTemp = []
    nL = len(pathWithArgs)
    n1 = n2 = 0
    bInQ = False
    bBreak = False;
    while not bBreak:
        n2 = pathWithArgs.find('"', n2)
        bBreak = (-1 == n2)
        if n1 < nL - 1:
            bInQ = not bInQ;
            if not bBreak:
                strTmp = pathWithArgs[n1:n2].strip()
            else:
                strTmp = pathWithArgs[n1:].strip()
            if (len(strTmp)):
                lstTemp.append((strTmp, not bInQ))
        if not bBreak:
            n1 = n2 = n2 + 1
    lstRet = []
    for val in lstTemp:
        if val[1]:
            lstRet.append(val[0])
        else:
            lstRet.extend(val[0].split())
    return lstRet

@tracing
def run( pathWithArgs ):
    if (isinstance(pathWithArgs, list)):
        pathWithArgsList = pathWithArgs;
    else:
        pathWithArgsList = _splitPathWithArgs(pathWithArgs)

    LDBG( "Run: " + " ".join(pathWithArgsList) )
    return pp.call(pathWithArgsList);
