#Lib: not executable
# -*- coding: UTF-8 -*-

import os

if os.environ.get("WEXT_COMMON_DEBUG"):
    print("import >>>", __file__)

import sys
import re

def isPython3():
    return sys.version_info[0] > 2

def ONLY_PYTHON3():
    if not isPython3():
        sys.stderr.write("Python %s is not supported!" % sys.version[:3])
        sys.exit()

class _env_set(object):
    def __init__(self):
        pass
'''
requere trace_env_init or/and define_trace_env call
'''
trace_env = _env_set()

def define_trace_env(var_name, default_val, env_name = None, reg_env_name = None):
    if isPython3():
        global trace_env
    setattr(trace_env, var_name, default_val)
    if env_name:
        try:
            ret = os.environ[env_name]
            if reg_env_name:
                ret = re.match(reg_env_name, ret).group(0)
            if isinstance(default_val, bool):
                rl = ret.lower()
                if rl in ["true", "1"]:
                    ret = True
                elif rl in ["false", "0", "null", "none"]:
                    ret = False
            else:
                ret = type(default_val)(ret)
            setattr(trace_env, var_name, ret)
        except Exception:
            pass

def trace_env_init():
    define_trace_env("debug", False, "DEBUG", r"(True|False)")
    define_trace_env("app_name", "*", "APP_NAME", r"[a-zA-Z]+[\d]*")

trace_env_init()

def formatVersion(major = 0, minor = 0, build = 0, patch = 0):
    ret = "%d.%d.%d" % (major, minor, build)
    if patch:
        ret = "%s.%d" % (ret, patch)
    return ret

def str2Bytes(data, encoding = 'utf-8'):
    if isinstance(data, bytes):
        return data
    elif not data or not isinstance(data, str):
        return b""
    return bytes(data, encoding = encoding)
def bytes2Str(data, encoding = 'utf-8'):
    if isinstance(data, str):
        return data
    elif not data or not isinstance(data, bytes):
        return ""
    return str(data, encoding = encoding)



