from setuptools import setup, find_packages

__version__ = 1.0

def read_file(filename):
    with open(filename) as f:
        return f.read()

setup(
    name='wext-common',
    version=__version__,
    description='WEXT Common Library: '
                'root library from WEXT series',
    author='Andrei Masilevich',
    author_email='a.masilevich@yandex.ru',
    long_description=read_file('README.md'),
    license=read_file('LICENSE'),
    keywords=[
        'library',
        'helpers',
        'common',
        'debug',
        'enviroment',
        'logging'],
    platforms=[
        'Ubuntu 16.04 x86 (tested)', 
        'Ubuntu 16.10 x86_64 (tested)', 
        'CentOS 6 AMD64', 
        'Windows 7 x64 (tested)'],
    packages=['wext_common'],
    package_dir = {'wext_common': 'wext_common'}, 
    entry_points={},
    test_suite='tests',
)
