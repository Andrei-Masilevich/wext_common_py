# -*- coding: UTF-8 -*-

from os.path import basename, dirname
import os

if os.environ.get("WEXT_COMMON_DEBUG"):
    print(">>>", basename(__file__).upper())

import os
import tempfile
import logging

from wext_common.liblog import *

class TestException(Exception):
    pass

def test():

    ls = ["server.session.SSL", 
          "server.session", 
          "server"]
    
    for _logName in ls:
        flBuff = tempfile.NamedTemporaryFile(mode='w+', prefix=basename(__file__), suffix= "." +  _logName + ".log", delete=False)
        path2puff = flBuff.name
        flBuff.close()
    
        _log = logging.getLogger(_logName)
        
        LDBG(path2puff)
        
        #write to files (in /tmp folder)
        _log.addRotatingFileHandler(path2puff, maxBytes = 1024, backupCount = 3);
        #write to stderr
        _log.addStreamHandler();
        #write to syslog
        _log.addLocalSysLogHandler();
        
        _log.debug("debug message")
        SLEEP(0.1)
        _log.info("info message")
        SLEEP(0.1)
        _log.warning("warning message")
        SLEEP(0.1)
        _log.error("error message")
        SLEEP(0.1)

    return True

if __name__ == "__main__":

    raise NotImplementedError
