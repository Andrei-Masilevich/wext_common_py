# -*- coding: UTF-8 -*-
        
from unittest import TestCase, main as start_tests

import sys
from os.path import dirname, join, normpath
sys.path.append(join(dirname(__file__), "..")) #to find tested lib

from os import environ as e
if e.get("WINGDB_ACTIVE"):
    if not e.get("DEBUG"):
        e["DEBUG"]="False"
    if not e.get("APP_NAME"):
        e["APP_NAME"]="WingIDETests"
    e["WEXT_COMMON_DEBUG"]='1'

from os import environ as e

_init = False

class tests(TestCase):
    def setUp(self):
        global _init
        if not _init:  #any objects for each test*
            _init = True
            print(sys.executable)

        '''
    def test1Liblog(self):
        from testLiblog import test
        self.assertEqual(test(), True)

    def test1Liblog2(self):
        from testLiblog2 import test
        self.assertEqual(test(), True)

        '''

    def test0(self):
        import wext_common
        dir(wext_common)
        from wext_common import trace_env as trEnv
        print("DEBUG = %s" % trEnv.debug)
        print("APP_NAME = %s" % trEnv.app_name)
        self.assertEqual(True, True)

    def test1Libtrace(self):
        from testLibtrace import test
        self.assertEqual(test(), True)

if __name__ == "__main__":
    try:            
        start_tests()
    except Exception as e:
        print(e)
    sys.exit(0)
    