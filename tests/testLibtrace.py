# -*- coding: UTF-8 -*-

from os.path import basename, dirname
import os

if os.environ.get("WEXT_COMMON_DEBUG"):
    print(">>>", basename(__file__).upper())

import datetime as dt

from wext_common.liblog import LMSG, LDBG, LWRN, LERR, SLEEP
import wext_common.libtrace as tr

@tr.tracing
def testTr(arg1, arg2 = None):
    LMSG("Execution with %r, %r", arg1, arg2)
    return arg1 
    
def test():

    testTr(1)
    testTr(1, 2)
    
    prof = tr.profiler()

    with prof:
        i = 0
        LMSG("time (%d) = %s", i, dt.datetime.now())
        i += 1
        SLEEP(0.2)
        LMSG("time (%d) = %s", i, dt.datetime.now())
        i += 1
        SLEEP(0.2)
        LMSG("time (%d) = %s", i, dt.datetime.now())
        i += 1
        SLEEP(0.2)
        LMSG("time (%d) = %s", i, dt.datetime.now())

    if prof:
        LMSG("".ljust(20, "="))
        LMSG("Profile report:")
        LMSG("\tStarted at: {}".format(prof.started()))
        LMSG("\tFinished at: {}".format(prof.finished()))
        LMSG("\tElapsed: {} s.".format(prof.elapsed()))

    return True

if __name__ == "__main__":

    raise NotImplementedError
