# -*- coding: UTF-8 -*-

from os.path import basename, dirname
import os

if os.environ.get("WEXT_COMMON_DEBUG"):
    print(">>>", basename(__file__).upper())

import os
import tempfile
import random as rnd
import logging

from wext_common.liblog import *

class TestException(Exception):
    pass

def test():

    rnd.seed()

    flBuff = tempfile.NamedTemporaryFile(mode='w+', prefix=basename(__file__), suffix=".log", delete=False)
    path2puff = flBuff.name
    flBuff.close()

    _log = logging.getLogger(__name__)
    
    try:
    
        LDBG(path2puff)
    
        #write to files (in /tmp folder)
        _log.addRotatingFileHandler(path2puff, maxBytes = 1024, backupCount = 3);
        #write to stderr
        _log.addStreamHandler();
        #write to syslog
        _log.addLocalSysLogHandler();
        
        lt = [_log.debug, _log.info, _log.warning, _log.error]
        for i in range(1, 20):
            if rnd.randint(0, 1):
                ri = rnd.randint(ord('a'), ord('z'))
            else:
                ri = rnd.randint(ord('A'), ord('Z'))
            ch = chr(ri)
            ri = rnd.randint(12, 120)
            log = rnd.choice(lt)
            log("%s", "".ljust(ri, ch))
            
        raise TestException
    
    except:
        _log.error("", exc_info = True)

    return True

if __name__ == "__main__":

    raise NotImplementedError
